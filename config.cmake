#--------------------------------------------------------
#   ASD global configuration
#--------------------------------------------------------

cmake_minimum_required(VERSION 3.19)

#--------------------------------------------------------

set(COMMON_COMPILE_FLAGS "${COMMON_COMPILE_FLAGS}")

# TODO: extract to a separate package with env variables

set(EMSCRIPTEN_MULTI_THREADED OFF)

# force compiler flags in installed dependencies
if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Emscripten" AND "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(COMMON_COMPILE_FLAGS "${COMMON_COMPILE_FLAGS} -s LLD_REPORT_UNDEFINED -s WASM=1 -s SAFE_HEAP=1 -s DISABLE_EXCEPTION_CATCHING=0 -g4 --bind")

    if (EMSCRIPTEN_MULTI_THREADED)
        set(COMMON_COMPILE_FLAGS "${COMMON_COMPILE_FLAGS} -pthread -s USE_PTHREADS=1 -s PTHREAD_POOL_SIZE=4")
    endif()

    set(CMAKE_EXECUTABLE_SUFFIX ".html")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMMON_COMPILE_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_COMPILE_FLAGS}")

set(CONAN_INSTALL_FLAGS ${CONAN_INSTALL_FLAGS} -e CFLAGS=${CMAKE_C_FLAGS} -e CXXFLAGS=${CMAKE_CXX_FLAGS})

#--------------------------------------------------------
