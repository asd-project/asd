from conans import ConanFile

class CertifyConan(ConanFile):
    name = "certify"
    version = "0.1.0"
    license = "boost"
    url = "https://djarek.github.io/certify/"
    description = "Boost.ASIO-based TLS certificate verification library"
    topics = ("cpp", "cpp11", "asio", "boost", "http", "tls", "tls-certificate")
    no_copy_source = True
    requires = (
        "boost/[>=1.77]",
        "openssl/1.1.1l"
    )

    def source(self):
        self.run("git clone https://github.com/djarek/certify.git certify")

    def package(self):
        self.copy("*.hpp", dst="include", src="certify/include")

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["certify/include"]
