from conans import ConanFile, tools

project_name = "ctti"

class CttiConan(ConanFile):
    name = project_name
    version = "0.0.2"
    url = "https://gitlab.com/Manu343726/ctti"
    topics = ()
    no_copy_source = True

    def source(self):
        self.run("git clone --branch 0.0.2 https://gitlab.com/Manu343726/ctti ctti")

        # fixes incorrect gcc version detection
        tools.replace_in_file("ctti/include/ctti/detail/language_features.hpp", "__GCC__", "__GNUC__")

    def package(self):
        self.copy("*.h", dst="include", src="ctti/include")
        self.copy("*.hpp", dst="include", src="ctti/include")

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["ctti/include"]
