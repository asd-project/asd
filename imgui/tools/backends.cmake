
set(IMGUI_ROOT "${CMAKE_CURRENT_LIST_DIR}/../")
file(RELATIVE_PATH IMGUI_ROOT "${CMAKE_CURRENT_SOURCE_DIR}" "${IMGUI_ROOT}")

function(imgui_backends)
    set(PREVIOUS_DOMAIN "${CURRENT_SOURCE_DOMAIN}")
    domain(.)

    if(CONAN_USER_ASD.IMGUI_IN_LOCAL_CACHE)
        group("${IMGUI_ROOT}/res/bindings" External)
    else()
        group("${IMGUI_ROOT}/source_subfolder/backends" External)
    endif()

    set(FILES)

    foreach(backend ${ARGN})
        set(FILES ${FILES} imgui_impl_${backend}.h imgui_impl_${backend}.cpp)
    endforeach()

    files(${FILES})

    domain("${PREVIOUS_DOMAIN}")
endfunction()
