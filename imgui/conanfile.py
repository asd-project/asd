
import os

from conans import ConanFile, CMake

project_name = "imgui"


class ImGuiConan(ConanFile):
    name = "asd.%s" % project_name
    version = "1.79.0"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/ocornut/imgui"
    description = "Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies"
    topics = ("conan", "imgui", "gui", "graphical")
    license = "MIT"
    exports_sources = ["CMakeLists.txt"]
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "source_subfolder/backends*", "tools*"
    options = {
        "shared": [True, False],
        "fPIC": [True, False]
    }
    default_options = {
        "shared": False,
        "fPIC": True
    }
    requires = (
        "asd.build_tools/0.0.1@asd/testing"
    )

    _cmake = None

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src=self._source_subfolder)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

        self.copy("*.cmake", dst="tools", src="tools")
        self.copy("imgui_impl_*", dst=os.path.join("res", "bindings"), src=os.path.join(self._source_subfolder, "backends"))
        self.copy("*.ttf", dst=os.path.join("res", "fonts"), src=os.path.join(self._source_subfolder, "misc", "fonts"))

        if self.settings.compiler == "Visual Studio":
            self.copy("*.natvis", dst=os.path.join("res", "natvis"), src=os.path.join(self._source_subfolder, "misc", "debuggers"))
        elif self.settings.compiler == "gcc":
            self.copy("*.gdb", dst=os.path.join("res", "gdb"), src=os.path.join(self._source_subfolder, "misc", "debuggers"))

        self.copy("LICENSE.txt", dst="licenses", src=self._source_subfolder)

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = [self._source_subfolder, os.path.join(self._source_subfolder, "backends")]
            self.cpp_info.srcdirs = [os.path.join(self._source_subfolder, "backends")]
        else:
            self.cpp_info.includedirs = ['include', os.path.join("res", "bindings")]
            self.cpp_info.srcdirs = [os.path.join("res", "bindings")]

        self.cpp_info.libs = [project_name]

        if self.settings.os == "Linux":
            self.cpp_info.system_libs.append("m")

        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
        self.user_info.IN_LOCAL_CACHE = self.in_local_cache
