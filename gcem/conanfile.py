
import os

from conans import ConanFile, CMake, tools

project_name = "gcem"


class GcemConan(ConanFile):
    name = project_name
    version = "1.13.1"
    url = "https://github.com/kthohr/gcem"
    topics = ()
    no_copy_source = True

    def source(self):
        self.run("git clone --depth 1 --branch v1.13.1 https://github.com/kthohr/gcem gcem")

    def package(self):
        self.copy("*.hpp", dst="include", src="gcem/include")

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["gcem/include"]
