# ASD
**ASD** is the multi-functional set of small libraries for C++ development packaged with workspace management environment. The main goal of this project is to make development rapid and clear, to avoid unnecessary complexity while maintaining the effectiveness of applications in the end. It is intended to be cross-platform (tested on Windows (msvc 19), Linux (gcc 8, clang 9), MacOS (apple-clang 11) and Emscripten). The focus is made on compile-time programming, so lots of templates can be found here. It helps to move the complexity from user space to library space.

The **ASD** project is inspired by Boost but is meant to provide more high-level components and tools to deal with graphics, user input, physics, 2D/3D scene management and so on. At this point, the development is at an early stage, so some of these components are slightly crude, and documentation is WIP. There is a lot of work to be done to make things work as intended, so any help will be __*VERY*__ appreciated.

These are some of the external libraries used as a foundation for the project parts:
- Boost: Preprocessor (for asd::meta), Filesystem, Asio (for asd::flow), Beast, Variant, Optional, PolyCollection and so on.
- SDL 2 - optional implementation of window class and inputs, all wrapped into [Boost].DI module to reduce initialization boilerplate.
- ImGUI - debugging ui.
- magic_enum - compile-time enum reflection.
- ctti - compile-time type identifiers.
- gcem - constexpr math.
- ...

## Features
### Common
- Mathematical objects and functions (matrices, vectors, quaternions, geometrical objects; SIMD support).
- Generalized graphic context with extensions (basic OpenGL backend is already implemented, Vulkan will be the next milestone).
- Application main loop and time flow control (based on boost::asio::io_context and timers). When targeting Emscripten, flow::run_main will run Emscripten [main loop](https://emscripten.org/docs/api_reference/emscripten.h.html#browser-execution-environment).  
- Scene management: scene container and objects.
- Basic UI (custom widgets with any properties: box-shadows, borders, etc.), input (mouse, keyboard), animations (number, color animation, custom easing support).

### Meta-programming
- Various meta-programming utilities for processing of type collections and type transformations.
- Meta-maps with support for compile-time string lookup, flags utilities and components.  
- Symbol-based parameters processing (see [symbol library](https://gitlab.com/asd-project/symbol)).
- Integration with the build system e.g. windows __dllimport__/__dllexport__ are managed automatically with the `export_api(module)` macro without additional boilerplate for each library.
- Assets management via custom build tools and compile-time assets collections.

Libraries in **ASD** are as small and independent as possible.   
It is meant to have different implementations of the same things (graphics components, windows, etc.), which share common/similar interfaces. ASD is not a framework, it is a tool set; it tries to move general design decisions to the user space by providing simple tools which are not tightly coupled.

## Build system
**ASD** provides the platform which helps you to manage your projects, their dependencies and assets to keep creating new libraries simple and painless.

Build system is based on [__*CMake*__](https://cmake.org/), [__*Conan*__](https://conan.io), CMake scripts and some system-dependent scripts. These tools will help you to create new libraries, add sources and dependencies in a fast way. CMakeLists files have more declarative style, Conan integration and dependencies management are completely hidden under the hood. There is also support for using scripts from dependencies via `package_include` (see `sdlpp/examples/sdl_test/CMakeLists.txt`).  

## TODOs
- Add more tests and examples.
- Add READMEs (with screenshots when possible).

## Future plans
- Integrate some ECS and provide built-in rendering and physical systems.
- Integrate or write assets management library.
- Implement application state management and routing which will allow to create nice interactive applications.

## Build instructions

ASD requires [__*CMake*__](https://cmake.org/download/) and [__*Conan*__](https://conan.io/downloads.html) installed (optional automatic installation is not implemented yet).  
After cloning repo via `git clone https://gitlab.com/bright-composite/asd` "build_tools" folder should be added to the PATH environment variable.
This makes it easier to perform ASD scripts lookup and to create custom workspaces where you can still use ASD tools.

`asd configure` will configure all nested packages as subprojects with multi-configuration support, mark them as [editable](https://docs.conan.io/en/latest/reference/commands/development/editable.html) and download all necessary dependencies.  
`asd configure -t=release|debug [-p=profile]` - configure subprojects with *Release* or *Debug* build type. Specify -p|--profile option to use custom Conan profile.  
You can also just use `cmake` command here.
  
`asd build [-t=debug|release] [-p=profile] <package>` - build a certain package. All dependencies will be built automatically.  
  
`asd run [-t=debug|release] [-p=profile] <package>` - run a certain target. Build task will be triggered automatically before run. When targeting Emscripten, a compiled page will be opened in a browser.  
  
`asd create <inline|library|application|test|example|benchmark|external> <package> [params...]` to create new package.  
Every new package contains CMakeLists.txt and conanfile.py files.  
Inline package source tree contains only header files.  
Library package is a simple static library with both headers and sources.  
Application, test, example and benchmark packages contain executable targets. Tests and benchmarks may take base package name as an additional parameter, examples require it.   
External package creation requires additional parameter with url, created package contains rules for fetching external project via `git clone`. Further configuration should be performed manually.

See [build_tools/cli.cmake](https://gitlab.com/asd-project/build_tools/-/blob/development/cli.cmake) for details.

Note: You can create custom workspaces by copying CMakeLists.txt from ASD repo root to the workspace root.   
