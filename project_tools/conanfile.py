import os

from conans import ConanFile, CMake

project_name = "project_tools"


class ProjectToolsConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.launch/0.0.1@asd/testing",
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*asd*", src="bin", dst="bin", keep_path=False)
