
import os

from conans import ConanFile, CMake, tools

project_name = "function_ref"


class FunctionRefConan(ConanFile):
    name = project_name
    version = "1.0.0"
    url = "https://github.com/TartanLlama/function_ref"
    topics = ()
    no_copy_source = True

    def source(self):
        self.run("git clone https://github.com/TartanLlama/function_ref function_ref")

    def package(self):
        self.copy("*.hpp", dst="include", src="function_ref/include")

    def package_info(self):
        if not self.in_local_cache:
            self.cpp_info.includedirs = ["function_ref/include"]
